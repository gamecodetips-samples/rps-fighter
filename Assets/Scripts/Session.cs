﻿namespace Assets.Scripts
{
    public class Session
    {
        public static bool CpuPlayerEnabled { get; private set; }

        public static void EnableCpuPlayer() => CpuPlayerEnabled = true;

        public static void DisableCpuPlayer() => CpuPlayerEnabled = false;
    }
}