﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Networking;
using Assets.Scripts.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Menu
{
    public class MultiplayerMenu : MonoBehaviour
    {
        public NetworkMenuManager NetworkMenuManager;
        public Text ConnectionStatus;
        public Text PlayerList;
        public Button CreateRoomButton;
        public GameObject PreRoom;
        public GameObject InRoom;
        public Button StartButton;
        public InputField NameField;
        public RoomButton RoomButtonPrefab;
        public Transform RoomButtonContainer;

        private GameObject previousScreen;

        public void OnConnect()
        {
            ConnectionStatus.text = "Connected to master server.";
            CreateRoomButton.interactable = true;
        }

        public void ShowRoom()
        {
            PreRoom.SetActive(false);
            InRoom.SetActive(true);
            StartButton.gameObject.SetActive(PhotonNetwork.IsMasterClient);
            StartButton.interactable = false;
        }

        public void StartFight() => SceneManager.LoadScene("OnlineFight");

        public void SetName()
        {
            var name = NameField.text;
            DataManager.SetPlayerName(name);
            PhotonNetwork.LocalPlayer.NickName = name;
        }

        public void Show(GameObject previousScreen)
        {
            this.previousScreen = previousScreen;
            previousScreen.SetActive(false);
            gameObject.SetActive(true);
            PreRoom.SetActive(true);
            InRoom.SetActive(false);
            NameField.text = DataManager.GetPlayerName();
            NetworkMenuManager.ConnectToMaster(this);
        }

        public void UpdateRoomList(List<RoomInfo> roomList)
        {
            ClearRooms();
            foreach (var room in roomList)
                AddRoom(room.Name);
        }

        void AddRoom(string roomName)
        {
            var button = Instantiate(RoomButtonPrefab, RoomButtonContainer);
            button.Initialize(roomName, NetworkMenuManager.JoinRoom);
        }

        void ClearRooms()
        {
            foreach(var btn in RoomButtonContainer.GetComponentsInChildren<RoomButton>())
                Destroy(btn.gameObject);
        }

        public void Back()
        {
            if (PhotonNetwork.InRoom)
                PhotonNetwork.LeaveRoom();
            if(PhotonNetwork.IsConnected)
                PhotonNetwork.Disconnect();

            gameObject.SetActive(false);
            previousScreen.SetActive(true);
        }

        public void ShowPlayers(IEnumerable<string> players)
        {
            PlayerList.text = string.Join("\n", players);
            SetStartButton(players.Count() > 1);
        }
        void SetStartButton(bool value) => 
            StartButton.interactable = value;
    }
}
