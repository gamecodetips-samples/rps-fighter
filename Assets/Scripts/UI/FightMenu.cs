﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class FightMenu : MonoBehaviour
    {
        public Text FightResult;
        public GameObject RematchButton;
        public PhotonView PhotonView;

        public void ShowAsFightEnded(bool localPlayerWon)
        {
            gameObject.SetActive(true);
            FightResult.gameObject.SetActive(true);
            FightResult.text = localPlayerWon ? "VICTORY" : "DEFEAT";

            RematchButton.SetActive( !PhotonNetwork.IsConnected || PhotonNetwork.IsMasterClient );
        }
        
        public void Rematch()
        {
            if (PhotonNetwork.IsConnected)
                PhotonView.RPC("ReloadScene", RpcTarget.Others);

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void Exit()
        {
            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.LeaveRoom();
                PhotonNetwork.Disconnect();
            }

            SceneManager.LoadScene("MainMenu");
        }

        [PunRPC]
        public void ReloadScene() =>
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        public void Switch() => 
            gameObject.SetActive(!gameObject.activeInHierarchy);
    }
}
