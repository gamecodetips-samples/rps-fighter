﻿using Assets.Scripts.Player;
using UnityEngine;

namespace Assets.Scripts.Directors
{
    public class FightDirector : MonoBehaviour
    {
        public PlayingCharacter Player1;
        public PlayingCharacter Player2;
        public RoundDirector RoundDirector;

        public bool AllAIOverride;

        protected virtual void Start()
        {
            if (AllAIOverride)
            {
                Player2.InitializeAsAi(RoundDirector);
                Player1.InitializeAsAi(RoundDirector);
            }
            else
            {
                Player1.InitializeAsHuman(RoundDirector);

                if (Session.CpuPlayerEnabled)
                    Player2.InitializeAsAi(RoundDirector);
                else
                    Player2.InitializeAsHuman(RoundDirector);
            }

            RoundDirector.Initialize(Player1, Player2);
        }
    }
}
