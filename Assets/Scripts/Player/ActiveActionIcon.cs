﻿using Assets.Scripts.Core;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class ActiveActionIcon : MonoBehaviour
    {
        public PlayerInputIcon Icon;
        public Animator IconAnimator;

        public void ShowAction(FightActionType action)
        {
            Icon.Initialize(action);
            IconAnimator.enabled = true;
            IconAnimator.SetTrigger("Reset");
        }
    }
}
